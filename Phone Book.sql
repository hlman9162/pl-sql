CREATE TABLE phone_book
(id number GENERATED ALWAYS AS IDENTITY primary key,
 first_name varchar2(30) NOT NULL,
 last_name varchar2(30) NOT NULL,
 father_name varchar2(30),
 workplace varchar2(30),
 email varchar2(30),
 mobile_number varchar2(30) NOT NULL,
 home_phone varchar2(30),
 UNIQUE(first_name,last_name,father_name));

set SERVEROUTPUT ON
CREATE OR REPLACE PACKAGE pkg_phone_book AS
    PROCEDURE add_new_reader
      (p_first_name    phone_book.first_name%type,
       p_last_name     phone_book.last_name%type,
       p_father_name   phone_book.father_name%type,
       p_workplace     phone_book.workplace%type,
       p_email         phone_book.email%type,
       p_mobile_number phone_book.mobile_number%type,
       p_home_phone    phone_book.home_phone%type,
       p_error         OUT number);
END;
/
CREATE OR REPLACE PACKAGE BODY pkg_phone_book AS
    PROCEDURE add_new_reader 
      (p_first_name    phone_book.first_name%type,
       p_last_name     phone_book.last_name%type,
       p_father_name   phone_book.father_name%type,
       p_workplace     phone_book.workplace%type,
       p_email         phone_book.email%type,
       p_mobile_number phone_book.mobile_number%type,
       p_home_phone    phone_book.home_phone%type,
       p_error         OUT number) IS
          
      
    BEGIN  
       
      INSERT INTO  phone_book
           (first_name,
            last_name,
            father_name,
            workplace,
            email,
            mobile_number,
            home_phone)
    VALUES (p_first_name,
            p_last_name,
            p_father_name,
            p_workplace,
            p_email,
            p_mobile_number,
            p_home_phone);
    COMMIT;
      EXCEPTION
       WHEN OTHERS 
        THEN 
        ROLLBACK;
        p_error:= sqlcode;
       
       
   END;
       
END;
SET SERVEROUTPUT ON
DECLARE
   v_error number;


BEGIN 
 pkg_phone_book.add_new_reader('Leman','Huseynli','Elxan','Sumqayitf','hlman9162f','07080807f0','287338f',v_error);
 dbms_output.put_line(v_error);
END;
