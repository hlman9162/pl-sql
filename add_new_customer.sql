Tapşırıq:
bank_customers table'na yeni datalar insert eden prosedure yazilamlidir
Şərtlər:
1.İD'ni sistemin ozu elave etmelidir
2.First_name ve Last_name unikal olmalidirlar
3.Email bos olmamilidir
4.Package daxilinde yazilamlidir
5.Sistemin qaytardigi xetalari gormeliyik

CREATE TABLE bank_customers
(id number GENERATED ALWAYS AS IDENTITY primary key,
 first_name varchar2(30),
 last_name varchar2(30),
 UNIQUE(first_name,last_name),
 email varchar2(50) not null);

set SERVEROUTPUT ON
CREATE OR REPLACE PACKAGE pkg_customers AS
    PROCEDURE add_new_customer 
      (p_first_name bank_customers.first_name%type,
       p_last_name bank_customers.last_name%type,
       p_email bank_customers.email%type);
END;
/
CREATE OR REPLACE PACKAGE BODY pkg_customers AS
    PROCEDURE add_new_customer 
      (p_first_name bank_customers.first_name%type,
       p_last_name bank_customers.last_name%type,
       p_email bank_customers.email%type) IS
       
      
    BEGIN  
      INSERT INTO  bank_customers
           (first_name,
            last_name,
            email)
          VALUES(p_first_name,
                 p_last_name,
                 p_email);
          COMMIT;
      EXCEPTION
        WHEN OTHERS 
        THEN 
        ROLLBACK;
        DBMS_OUTPUT.PUT_LINE(sqlerrm);
        
   END;
       
END;

EXEC pkg_customers.add_new_customer('Leman','Huseynli','kjsdkfj');
EXEC pkg_customers.add_new_customer('Leman','Huseynli');
EXEC pkg_customers.add_new_customer('Leman','Huseynli','jewhlha');

SELECT*FROM bank_customers;
